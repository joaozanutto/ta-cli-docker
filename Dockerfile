FROM debian

COPY . /root

RUN apt update && apt install bash curl -y \
    && curl -Ls https://github.com/testappio/cli/releases/latest/download/install | bash
