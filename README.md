# Test App CLI Docker

This Docker image was created to run the Test App CLI in a CI script with ease.

## Recommended usage

I recommend using this image with the `sed`command in the CI pipeline as a find and replace tool to update the tokens in the .ta-cli.json file
